//
//  ViewController.m
//  SwipeScrollDemo
//
//  Created by DingXiao on 2016/12/6.
//  Copyright © 2016年 Dennis. All rights reserved.
//

#import "ViewController.h"
#import "SwipeView.h"

@interface ViewController () <SwipeDelegate, SwipeViewDataSource>

@property (nonatomic, strong) SwipeView *swipeView;
@property (assign, nonatomic) NSInteger count;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor darkGrayColor];
    self.count = 5;
    self.swipeView.enableCycleScroll = NO;
    [self swipeView];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)swipeViewNumberOfCards:(SwipeView *)swipeView {
    return self.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView
          cardAtIndex:(NSUInteger)index {
    NSString *imageName = [NSString stringWithFormat:@"Card_like_%@",@(index + 1)];
    UILabel *labvel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.swipeView.frame.size.width, self.swipeView.frame.size.height)];
    labvel.text = imageName;
    labvel.backgroundColor = [UIColor whiteColor];
    //    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    NSLog(@"%@",imageName);
    return labvel;
}

- (void)swipeView:(SwipeView *)swipeView didSwipeCardAtIndex:(NSUInteger)index inDirection:(SwipeDirection)direction {
    
}

- (void)swipeViewDidRunOutOfCards:(SwipeView *)swipeView {
    [swipeView resetCurrentCardNumber];
}

- (void)swipeView:(SwipeView *)swipeView didSelectCardAtIndex:(NSUInteger)index
{
    NSLog(@"点击");
}

- (BOOL)swipeViewShouldApplyAppearAnimation:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldMoveBackgroundCard:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldTransparentizeNextCard:(SwipeView *)swipeView
{
    return YES;
}

- (POPPropertyAnimation *)swipeViewBackgroundCardAnimation:(SwipeView *)swipeView
{
    return nil;
}


#pragma mark - getter 

- (SwipeView *)swipeView {
    if (!_swipeView) {
        _swipeView = [[SwipeView alloc] initWithFrame:CGRectMake(40, 100, [[UIScreen mainScreen] bounds].size.width-80, 200)];
        _swipeView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_swipeView];
        _swipeView.delegate = self;
        _swipeView.dataSource = self;
    }
    return _swipeView;
}


@end
